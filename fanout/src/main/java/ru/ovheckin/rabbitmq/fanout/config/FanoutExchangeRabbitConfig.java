package ru.ovheckin.rabbitmq.fanout.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ovheckin.rabbitmq.fanout.consts.Constant;

@Configuration
public class FanoutExchangeRabbitConfig {

    @Value("${spring.rabbitmq.template.exchange}")
    private String fanoutExchangeName;

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(fanoutExchangeName);
    }

    @Bean
    public Queue queueFirst() {
        return new Queue(Constant.FIRST_QUEUE, false);
    }

    @Bean
    public Queue queueSecond() {
        return new Queue(Constant.SECOND_QUEUE, false);
    }

    @Bean
    public Binding bindingFirst() {
        return BindingBuilder.bind(queueFirst()).to(fanoutExchange());
    }

    @Bean
    public Binding bindingSecond() {
        return BindingBuilder.bind(queueSecond()).to(fanoutExchange());
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}