package ru.ovheckin.rabbitmq.fanout.consts;

public interface Constant {

    String FIRST_QUEUE = "two-consumers-queue";

    String SECOND_QUEUE = "two-consumers-queue-second";
}
