package ru.ovheckin.rabbitmq.fanout.listener;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.ovheckin.rabbitmq.fanout.consts.Constant;
import ru.ovheckin.rabbitmq.fanout.dto.MessageDTO;

@Slf4j
@Component
public class Consumer {

    @RabbitListener(queues = Constant.FIRST_QUEUE)
    public void handleFanoutFirst(final @NotNull MessageDTO messageDTO) {
        log.info("FirstHandler_FirstQueue ===== " + messageDTO.toString());
    }

    @RabbitListener(queues = Constant.FIRST_QUEUE)
    public void handleFanoutSecond(final @NotNull MessageDTO messageDTO) {
        log.info("SecondHandler_FirstQueue ===== " + messageDTO.toString());
    }

    @RabbitListener(queues = Constant.SECOND_QUEUE)
    public void handleFanoutSecondQueue(final @NotNull MessageDTO messageDTO) {
        log.info("SecondHandler_SecondQueue ===== " + messageDTO.toString());
    }

}