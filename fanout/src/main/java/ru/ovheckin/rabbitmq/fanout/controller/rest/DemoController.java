package ru.ovheckin.rabbitmq.fanout.controller.rest;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ovheckin.rabbitmq.fanout.dto.MessageDTO;

@RestController
@RequestMapping("/fanout")
public class DemoController {

    @Autowired
    private RabbitTemplate template;

    @PostMapping("/post")
    public String postMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic
    ) {
        template.convertAndSend(topic, messageDTO);
        return "Success! Message is processing...";
    }
}