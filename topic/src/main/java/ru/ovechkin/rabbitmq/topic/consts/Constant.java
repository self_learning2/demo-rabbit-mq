package ru.ovechkin.rabbitmq.topic.consts;

public final class Constant {

    public static final String FIRST_QUEUE = "queue-with-one-routing-key";

    public static final String SECOND_QUEUE = "queue-with-two-routing-keys";

    public static final String ERROR_ROUTING_KEY = "*.error.*";

    public static final String INFO_ROUTING_KEY = "*.*.info";

    public static final String WARNING_ROUTING_KEY = "warning.#";

    private Constant() {
    }

}