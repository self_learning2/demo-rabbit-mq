package ru.ovechkin.rabbitmq.topic.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.rabbitmq.topic.dto.MessageDTO;

@Slf4j
@RestController
@RequestMapping("/topic/post")
public class DemoController {

    @Autowired
    private RabbitTemplate template;

    /**
     * http://localhost:8080/topic/post/info?routing_key=te.st.info
     */
    @PostMapping("/info")
    public String postInfoMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic,
            @RequestParam("routing_key") final String infoRoutingKey
    ) {
        log.info(infoRoutingKey);
        template.convertAndSend(topic, infoRoutingKey, messageDTO);
        return "Success! ===INFO=== Message is processing...";
    }

    /**
     * http://localhost:8080/topic/post/warning?routing_key=warning.t.e.s.t
     */
    @PostMapping("/warning")
    public String postWarningMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic,
            @RequestParam("routing_key") final String warningRoutingKey
    ) {
        log.info(warningRoutingKey);
        template.convertAndSend(topic, warningRoutingKey, messageDTO);
        return "Success! ===WARNING=== Message is processing...";
    }

    /**
     * http://localhost:8080/topic/post/error?routing_key=test.error.test
     */
    @PostMapping("/error")
    public String postMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic,
            @RequestParam("routing_key") final String errorRoutingKey
    ) {
        log.info(errorRoutingKey);
        template.convertAndSend(topic, errorRoutingKey, messageDTO);
        return "Success! ===ERROR=== Message is processing...";
    }

}