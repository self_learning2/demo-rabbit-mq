package ru.ovechkin.rabbitmq.demo.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ovechkin.rabbitmq.demo.api.repository.ReviewRepository;
import ru.ovechkin.rabbitmq.demo.api.srervice.IReviewService;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;
import ru.ovechkin.rabbitmq.demo.entity.Review;
import ru.ovechkin.rabbitmq.demo.exception.IdEmptyException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewIsNullException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewService implements IReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public void create(@Nullable final ReviewDTO reviewDTO) throws ReviewIsNullException {
        if (reviewDTO == null) throw new ReviewIsNullException();
        @NotNull final Review review = reviewDTO.toEntity();
        if (review == null) throw new ReviewIsNullException();
        reviewRepository.save(review);
    }

    @NotNull
    @Override
    public ReviewDTO findById(@Nullable final String id) throws IdEmptyException, ReviewNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Review review = reviewRepository.findById(id).orElse(null);
        if (review == null) throw new ReviewNotFoundException();
        return review.toDTO();
    }

    @Override
    public List<ReviewDTO> findAll() {
        return reviewRepository
                .findAll()
                .stream()
                .map(Review::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void updateById(
            @Nullable final String id,
            @Nullable final ReviewDTO reviewDTO
    ) throws ReviewNotFoundException, IdEmptyException, ReviewIsNullException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (reviewDTO == null) throw new ReviewNotFoundException();
        @NotNull final Review review = reviewDTO.toEntity();
        if (review == null) throw new ReviewIsNullException();
        review.setId(id);
        reviewRepository.save(review);
    }

    @Override
    public void deleteById(@Nullable final String id) throws IdEmptyException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        reviewRepository.deleteById(id);
    }

}