package ru.ovechkin.rabbitmq.demo.listener;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.rabbitmq.demo.api.repository.ReviewRepository;
import ru.ovechkin.rabbitmq.demo.consts.Constant;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;
import ru.ovechkin.rabbitmq.demo.entity.Review;
import ru.ovechkin.rabbitmq.demo.exception.IdEmptyException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewIsNullException;

@Slf4j
@Component
public class ReviewListener {

    @Autowired
    private ReviewRepository reviewRepository;

    @RabbitListener(queues = Constant.DELETE_QUEUE)
    public void deleteById(@Nullable final String reviewId) throws IdEmptyException {
        if (reviewId == null || reviewId.isEmpty()) throw new IdEmptyException();
        reviewRepository.deleteById(reviewId);
    }

    @RabbitListener(queues = Constant.CREATE_QUEUE)
    public void create(@Nullable final ReviewDTO reviewDTO) throws ReviewIsNullException {
        if (reviewDTO == null) throw new ReviewIsNullException();
        log.info(reviewDTO.toString());
        @NotNull final Review review = reviewDTO.toEntity();
        if (review == null) throw new ReviewIsNullException();
        reviewRepository.save(review);
    }

}