package ru.ovechkin.rabbitmq.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "review_rabbitmq")
public class Review {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @Nullable
    private String name;

    @Column
    @Nullable
    private String text;

    @Column
    @Nullable
    private Integer score;

    public ReviewDTO toDTO() {
        return new ReviewDTO(id, name, text, score);
    }

}