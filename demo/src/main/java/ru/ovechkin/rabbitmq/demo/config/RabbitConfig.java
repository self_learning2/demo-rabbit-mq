package ru.ovechkin.rabbitmq.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ovechkin.rabbitmq.demo.consts.Constant;

@Configuration
public class RabbitConfig {

    @Value("${spring.rabbitmq.template.exchange}")
    private String topicExchangeName;

    @Bean
    public Queue createQueue() {
        return new Queue(Constant.CREATE_QUEUE, false);
    }

    @Bean
    public Queue deleteQueue() {
        return new Queue(Constant.DELETE_QUEUE, false);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    public Binding bindingCreateQueue(final Queue createQueue, final TopicExchange topicExchange) {
        return BindingBuilder.bind(createQueue).to(topicExchange).with(Constant.CREATE_KEY);
    }

    @Bean
    public Binding bindingDeleteQueue(final Queue deleteQueue, final TopicExchange topicExchange) {
        return BindingBuilder.bind(deleteQueue).to(topicExchange).with(Constant.DELETE_KEY);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}