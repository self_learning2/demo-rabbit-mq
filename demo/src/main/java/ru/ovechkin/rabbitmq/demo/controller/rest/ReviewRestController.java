package ru.ovechkin.rabbitmq.demo.controller.rest;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.rabbitmq.demo.consts.Constant;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;

@RestController
@RequestMapping("/api/review")
public class ReviewRestController {

    @Autowired
    private RabbitTemplate template;

    @Value("${spring.rabbitmq.template.exchange}")
    private String topic;

    @PostMapping("/post")
    public String postReview(
            @RequestBody final ReviewDTO reviewDTO
    ) {
        template.convertAndSend(topic, Constant.CREATE_KEY, reviewDTO);
        return "Success! Message is processing...";
    }

    @DeleteMapping("/remove/{id}")
    public String remove(
            @PathVariable("id") final String reviewId
    ) {
        template.convertAndSend(topic, Constant.DELETE_KEY, reviewId);
        return Constant.REDIRECT;
    }

}