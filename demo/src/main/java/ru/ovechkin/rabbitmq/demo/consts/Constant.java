package ru.ovechkin.rabbitmq.demo.consts;

public final class Constant {

    public static final String CREATE_QUEUE = "create-queue";

    public static final String DELETE_QUEUE = "delete-queue";

    public static final String CREATE_KEY = "create-key";

    public static final String DELETE_KEY = "delete-key";

    public static final String REDIRECT = "redirect:/review/main";

    private Constant() {
    }

}
