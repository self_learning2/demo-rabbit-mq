package ru.ovechkin.rabbitmq.direct.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ovechkin.rabbitmq.direct.consts.Constant;

@Configuration
public class FanoutExchangeRabbitConfig {

    @Value("${spring.rabbitmq.template.exchange}")
    private String directExchange;

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(directExchange);
    }

    @Bean
    public Queue queueFirst() {
        return new Queue(Constant.FIRST_QUEUE, false);
    }

    @Bean
    public Queue queueSecond() {
        return new Queue(Constant.SECOND_QUEUE, false);
    }

    @Bean
    public Binding errorBindingFirstQueue() {
        return BindingBuilder
                .bind(queueFirst())
                .to(directExchange())
                .with(Constant.ERROR_ROUTING_KEY);
    }

    @Bean
    public Binding errorBindingSecondQueue() {
        return BindingBuilder
                .bind(queueSecond())
                .to(directExchange())
                .with(Constant.ERROR_ROUTING_KEY);
    }

    @Bean
    public Binding infoBindingSecondQueue() {
        return BindingBuilder
                .bind(queueSecond())
                .to(directExchange())
                .with(Constant.INFO_ROUTING_KEY);
    }

    @Bean
    public Binding warningBindingSecondQueue() {
        return BindingBuilder
                .bind(queueSecond())
                .to(directExchange())
                .with(Constant.WARNING_ROUTING_KEY);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}