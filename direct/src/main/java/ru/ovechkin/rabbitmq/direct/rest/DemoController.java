package ru.ovechkin.rabbitmq.direct.rest;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ovechkin.rabbitmq.direct.consts.Constant;
import ru.ovechkin.rabbitmq.direct.dto.MessageDTO;

@RestController
@RequestMapping("/direct/post")
public class DemoController {

    @Autowired
    private RabbitTemplate template;

    @PostMapping("/info")
    public String postInfoMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic
    ) {
        template.convertAndSend(topic, Constant.INFO_ROUTING_KEY, messageDTO);
        return "Success! ===INFO=== Message is processing...";
    }

    @PostMapping("/warning")
    public String postWarningMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic
    ) {
        template.convertAndSend(topic, Constant.WARNING_ROUTING_KEY, messageDTO);
        return "Success! ===WARNING=== Message is processing...";
    }

    @PostMapping("/error")
    public String postMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic
    ) {
        template.convertAndSend(topic, Constant.ERROR_ROUTING_KEY, messageDTO);
        return "Success! ===ERROR=== Message is processing...";
    }

}