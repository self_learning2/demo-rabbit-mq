package ru.ovechkin.rabbitmq.direct.listener;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.rabbitmq.direct.consts.Constant;
import ru.ovechkin.rabbitmq.direct.dto.MessageDTO;

@Slf4j
@Component
public class Consumer {

    @RabbitListener(queues = Constant.FIRST_QUEUE)
    public void handleFanoutFirst(final @NotNull MessageDTO messageDTO) {
        log.info("FirstHandler_FirstQueue ===== " + messageDTO.toString());
    }

    @RabbitListener(queues = Constant.SECOND_QUEUE)
    public void handleFanoutSecondQueue(final @NotNull MessageDTO messageDTO) {
        log.info("FirstHandler_SecondQueue ===== " + messageDTO.toString());
    }

}