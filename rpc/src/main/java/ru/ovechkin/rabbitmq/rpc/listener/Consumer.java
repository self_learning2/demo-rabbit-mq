package ru.ovechkin.rabbitmq.rpc.listener;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.rabbitmq.rpc.consts.Constant;
import ru.ovechkin.rabbitmq.rpc.dto.MessageDTO;

@Slf4j
@Component
public class Consumer {

    @RabbitListener(queues = Constant.FIRST_QUEUE)
    public String handleTopicFirst(final @NotNull MessageDTO messageDTO) {
        log.info("FirstHandler_FirstQueue ===== " + messageDTO.toString());
        return ("_FirstHandler , " + messageDTO.toString() + "_");
    }

    @RabbitListener(queues = Constant.SECOND_QUEUE)
    public String handleTopicSecondQueue(final @NotNull MessageDTO messageDTO) {
        log.info("FirstHandler_SecondQueue ===== " + messageDTO.toString());
        return ("_SecondHandler , " + messageDTO.toString() + "_");
    }

}