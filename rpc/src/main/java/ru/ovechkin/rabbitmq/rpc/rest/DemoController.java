package ru.ovechkin.rabbitmq.rpc.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.rabbitmq.rpc.dto.MessageDTO;

@Slf4j
@RestController
@RequestMapping("/topic/post")
public class DemoController {

    @Autowired
    private RabbitTemplate template;

    /**
     * http://localhost:8080/topic/post/info?routing_key=te.st.info
     */
    @PostMapping("/info")
    public String postInfoMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic,
            @RequestParam("routing_key") final String infoRoutingKey
    ) {
        log.info(infoRoutingKey);
        return ("Received ++INFO++ from Server: ===" +
                template.convertSendAndReceive(topic, infoRoutingKey, messageDTO) + "===");
    }

    /**
     * http://localhost:8080/topic/post/warning?routing_key=warning.t.e.s.t
     */
    @PostMapping("/warning")
    public String postWarningMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic,
            @RequestParam("routing_key") final String warningRoutingKey
    ) {
        log.info(warningRoutingKey);
        return ("Received ++WARNING++ from Server: ===" +
                template.convertSendAndReceive(topic, warningRoutingKey, messageDTO) + "===");
    }

    /**
     * http://localhost:8080/topic/post/error?routing_key=test.error.test
     */
    @PostMapping("/error")
    public String postMessage(
            @RequestBody final MessageDTO messageDTO,
            @Value("${spring.rabbitmq.template.exchange}") final String topic,
            @RequestParam("routing_key") final String errorRoutingKey
    ) {
        log.info(errorRoutingKey);
        return ("Received ++ERROR++ from Server: ===" +
                template.convertSendAndReceive(topic, errorRoutingKey, messageDTO) + "===");
    }

}